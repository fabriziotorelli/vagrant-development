#!/bin/sh

#install docker
sudo apt-get update

sudo apt-get -y install curl \
    linux-image-extra-$(uname -r) \
    linux-image-extra-virtual

sudo apt-get -y install apt-transport-https \
                           ca-certificates

sudo curl -fsSL https://yum.dockerproject.org/gpg | sudo apt-key add -

#verify the new keys
sudo apt-key fingerprint 58118E89F3A912897C070ADBF76221572C52609D

sudo add-apt-repository \
       "deb https://apt.dockerproject.org/repo/ \
       ubuntu-$(lsb_release -cs) \
       main"

sudo apt-get update

sudo apt-get -y install docker-engine

#Install docker machine
sudo curl -L https://github.com/docker/machine/releases/download/v0.9.0/docker-machine-`uname -s`-`uname -m` >/tmp/docker-machine &&
  sudo chmod +x /tmp/docker-machine &&
  sudo cp /tmp/docker-machine /usr/local/bin/docker-machine


#Install docker compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose &&
   sudo chmod +x /usr/local/bin/docker-compose

#Clean the unused space
apt-get -y autoremove && \
    apt-get -y clean && \
    rm -rf /var/lib/apt/lists/*

#Verify the full installation
sudo docker version
sudo docker-machine version
sudo docker-compose --version

echo "Copying new commands ...."
sudo chmod 777 /home/ubuntu/clean-*
sudo cp /home/ubuntu/clean-* /usr/bin/

sudo chmod 777 /home/ubuntu/docker-*
sudo cp /home/ubuntu/docker-* /usr/bin/

echo "Cleaning installation files ...."
sudo rm -f /home/ubuntu/clean-*
sudo rm -f /home/ubuntu/docker-*


echo "Try to clean empty blocks ...."

clean-space
