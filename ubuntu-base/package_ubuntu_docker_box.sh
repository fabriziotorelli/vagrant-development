#!/bin/sh
MACHINE_ID=$(VBoxManage list vms | grep 'ubuntu-docker-base' | awk 'BEGIN {FS = OFS = " "} {print $2}' | awk 'BEGIN {FS = OFS = "{"} {print $2}' | awk 'BEGIN {FS = OFS = "}"} {print $1}')
echo $MACHINE_ID
if ! [[ -z $MACHINE_ID ]]; then
  echo "FOUND ID : $MACHINE_ID"
  echo "Proceeding with packaging ..."
  rm -f box/ubuntu-docker-1.13.0-base-box.box
  rm -f docker-commands.tar
  tar -cvf docker-commands.tar clean-install clean-space docker-compose-su docker-machine-su docker-su install_docker.sh
  vagrant package --base $MACHINE_ID --output box/ubuntu-docker-1.13.0-base-box.box --vagrantfile _Vagrantfile --include docker-commands.tar
else
  echo "Unable to locate machine ubuntu-base-1.*!!"
fi
