# Vagrant Machine Development

## Synopsis

Here a simple project providing vagrant scripts to create *vagrant compact machines* for a *virtual box provider*
[Code On](https://bitbucket.org/fabriziotorelli/vagrant-development.git)

## Code Sample

It is provided at the moment :

* Base vagrant ubuntu XENIAL 64-bit machine with the provisioning of the *Virtual Box Additions* ([*base*](/base)))
* Base ubuntu Docker machine (depending on the [*base*](/base)) provided of docker, docker-compose, docker-machine ([*ubuntu-base*](/ubuntu-base))
* Simple mandatory format to create an instance of the latest docker (now 1.13.0) machine instance, using the default, mandatory in case of provisioning control, box name variable [VAGRANT_BOX_NAME] for all the boxes ([*test-docker-machine*](/test-docker-machine))
* [Rancher OS machine definition on a writable disk](/rancher-os) from the Rancher OS ISO, and a specific [Rancher Server Vagrant Machines Workshop](/rancher-os/workshop)
* Cloudbees test ubuntu docker machine in folder [cloudbees-vagrant](https://bitbucket.org/fabriziotorelli/cloudbees-vagrant)


## Motivation

This project realize a sample and fast approach to share vagrant machines in the WIPRO digital technical department.


## Installation

For any command not specified you can refer to the `Vagrant Commands` section.

The hierarchy for the vagrant machine definition is :

* [*base*](/base), execute the start-up of vagrant with the command `vagrant up --provision --provider virtualbox`, execute the halt of the vagrant machine with the command `vagrant halt` , execute the packaging script [package_ubuntu_base_box.sh](/base/package_ubuntu_base_box.sh) and the creation script [make_base_ubuntu_box.sh](/base/make_base_ubuntu_box.sh) of base vagrant box, finally you can destroy the vagrant machine with the command `vagrant destroy -f`
* [*ubuntu-base*](/ubuntu-base), execute the start-up of vagrant with the command `vagrant up --provision --provider virtualbox`, execute the halt of the vagrant machine with the command `vagrant halt` , execute the packaging script [package_ubuntu_docker_box.sh](/ubuntu-bas/package_ubuntu_docker_box.sh) and the creation script [make_ubuntu_docker_box.sh](/ubuntu-bas/make_ubuntu_docker_box.sh) of base vagrant box, finally you can destroy the vagrant machine with the command `vagrant destroy -f`
* [*test-docker-machine*](/test-docker-machine), execute the start-up of vagrant with the command `vagrant up --provision --provider virtualbox`, then enjoy the experience. To access the vagrant machine you can use the command : `vagrant ssh`


## Vagrant Commands

Please install vagrant, virtual box and virtual box additions (Specific mac releases have been provided in the folder *mac-sw*), the init your vagrant plugins running the script : [*init-vagrant-plugins.sh*](/init-vagrant-plugins.sh) in the root path of the project. In any development project have been provided box packaging and install scripts.

To run a machine in the folder type :

`vagrant up --provision --provider virtualbox`


To stop the machine type :

`vagrant halt`


To restart the machine type :

`vagrant reload`

## Rancher OS

You can fins more about Rancher OS sample installation procedures at :

[Rancher OS Section](/rancher-os)


## Contributors

You can access to the issue tracker at:

[Vagrant Development Issue Tracker](https://bitbucket.org/fabriziotorelli/vagrant-development/issues)


## License

[MIT License](/License.md)
