#!/bin/sh
MACHINE_ID=$(VBoxManage list vms | grep 'ubuntu-base-x64' | awk 'BEGIN {FS = OFS = " "} {print $2}' | awk 'BEGIN {FS = OFS = "{"} {print $2}' | awk 'BEGIN {FS = OFS = "}"} {print $1}')
echo $MACHINE_ID
if ! [[ -z $MACHINE_ID ]]; then
  echo "FOUND ID : $MACHINE_ID"
  echo "Proceeding with packaging ..."
  rm -f box/ubuntu-base-box.box
  vagrant package --base $MACHINE_ID --output box/ubuntu-base-box.box --vagrantfile _Vagrantfile
else
  echo "Unable to locate machine ubuntu-base-x64.*!!"
fi
