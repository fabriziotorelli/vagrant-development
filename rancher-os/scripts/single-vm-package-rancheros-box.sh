#!/bin/sh
MACHINE_ID=$(VBoxManage list vms | grep 'rancheros-iso' | awk 'BEGIN {FS = OFS = " "} {print $2}' | awk 'BEGIN {FS = OFS = "{"} {print $2}' | awk 'BEGIN {FS = OFS = "}"} {print $1}')
echo $MACHINE_ID
if ! [[ -z $MACHINE_ID ]]; then
  echo "FOUND ID : $MACHINE_ID"
  echo "Proceeding with packaging ..."
  rm -f ../box/single-vm-rancheros-box.box
  vagrant package --base $MACHINE_ID --output ../box/single-vm-rancheros-box.box --vagrantfile ../templates/_Vagrantfile_single --include ../vagrant-rancheros-vbadditions-plugin.rb
else
  echo "Unable to locate machine rancheros-iso.*!!"
fi
