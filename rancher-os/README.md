# Rancher OS, On Writable Disk, Installation Guide

## Synopsis

This section explain step by step how-to install a rancher os virtual machine, starting from the rancher os iso image.

This project is part of the project [vagrant-development](/)

## Procedure

It is provided at the moment :

* Base Rancher OS on disk writable operating system Installation guide
* Sample single and multiple instance Vagrantfile folders
* Templates for single and multiple rancher os instances
* Workshop including one RancherOS System and a couple of ubuntu docker machines based on the box created in the main project area [Ubuntu Docker Vagrant Machine](https://bitbucket.org/fabriziotorelli/vagrant-development.git)


## Prerequisites

The installation requires Virtual Box, Vagrant, the Virtual Box Guest Additions.

Prepare a Virtual Box Virtual Machine with these characteristics over the default ones :
* General:
1. Basic -> OS Type : Linux
2. Basic -> OS Version: Other Linux (64-bit)


* System:
1. Motherboard -> Memory 2GB and Boot sequence : HDD, Optical
2. Acceleration -> Paravirtualization: legacy, Nested Paging : Enabled

* Storage:
1. Define Controller: IDE
2. Controller: HDD XXGB SOLID STATE DRIVE
3. Controller: Optical: rancheros-iso-rcXX.iso PRIMARY
3. Controller: Optical: default-config-image.iso SECONDARY

* Network:
1. eth0 : NAT (Define a NATNetwork adapter in Settings -> Network, supporting IP-V6 addresses)
1. eth1 : HostOnly -> cboxnet0 (with DHCP => server : 10.0.2.2, mask: 255.255.255.0, ip from : 192.168.50.100 to 192.168.50.255 )

Initially the machine is out of vagrant scope in this way we have a procedure to create a configuration iso image for the global installation configuration. We will define how-to create and use it later.


## Preparation

Ensure to have installed Virtual Box 5.1.XX, the Virtual Box Guest Additions 5.1.XX and Vagrant from the mac-sw folder.


## Installation

The steps:

* Customize the file [*cloud-config-install.yml*](/rancher-os/config/cloud-config-install.yml) and insert the proper authorization key, according to your public key (e.g. id_rsa.pub) at the end of the file where you find `- ssh AAA...`
* Pack the file in an iso ( [*make-config-iso.sh*][/rancher-os/scripts/make-config-iso.sh] )
* Create a Virtual Box Machine and a SCSI Adapter with a solid state disk of at least 20GB (as `primary master` I used 40GB, you can increase it in a further time), the iso image (e.g.: the provided *rancheros-v0.8.0-rc3.iso*, as `primary slave`) and the previous created configuration iso image *default-config-image.iso*  (as `secondary ...`). You can find some utilities in the iso.
* Start the machine and access to the auto-logged in RancherOS command shell.
* Mount the configuration iso (you can use the command `sudo su -` and then `mkdir -p /media/cdrom`, then `mount -t iso9660 /dev/sr1 /media/cdrom`).
* As root copy the cloud config file from */media/cd-rom* into the home folder (`cp /media/cd-rom/cloud-config-install.yml /root/cloud-config.yml`), assign the 777 credential to the file (`chmod 777 /root/cloud-config.yml`).
* As root verify if the new configuration is valid (`ros config validate -i /root/cloud-config.yml`)
* As root unmount the disk `sudo umount /media/cd-rom`, removing the not needed folder (`rm -Rf /media/cd-rom`).
* As root merge the config file to the current (temporary) rancher os  (`ros config merge -i /root/cloud-config.yml`).
* As root verify id the merged configuration is valid (`ros config export`), if it works you are ready to install the solid disk.

* Run the install command line clause:

`sudo ros install --cloud-config /root/cloud-config-install.yml --device /dev/sda --install-type generic`

Then answer `yes` to both the questions .....

* Now after the restart use your private key to login and test if the installation went properly.

`ssh -i ~/.ssh/id_rsa rancher@192.168.50.111` or `vagrant ssh`

* Once you logged in the rancheros you can change the main passwords with commands `sudo passwd rancher` and `sudo passwd root`. Now you have your recovery password in case of issues.

* You can power-off the machine `sudo poweroff 0` ad you can start packaging the environment (eg. [*row-package-rancheros-box.sh*](/rancher-os/scripts/row-package-rancheros-box.sh)) and install the box in the system ready to be imported and create the base rancher os environment (eg. [*row-install-rancheros-box.sh*](/rancher-os/scripts/row-install-rancheros-box.sh)). **Keep**closed**Virtual**Box**GUI**during**box**packaging**


## Packaging a base box

I have created some sample scripts to package the base samples box from the single and multiple machine virtual machine images and install the box in vagrant, the shell commands are in the [*scripts*](/rancher-os/scripts) folder.

The vagrant framework doesn't know the rancher os command set, to cover this issue I have provided a ruby vagrant supply command set, to import in the vagrant file (vagrant-rancheros-vbadditions-plugin.rb) in :

* [*base-rancheros-box*](/rancher-os/vagrant-rancheros-vbadditions-plugin.rb)
* [*test-single-rancheros*](/rancher-os/test-single-rancheros/vagrant-rancheros-vbadditions-plugin.rb)
* [*test-multi-rancheros*](/rancher-os/test-multi-rancheros/vagrant-rancheros-vbadditions-plugin.rb)
* [*workshop/rancher-server*](/rancher-os/workshop/rancheros-server/vagrant-rancheros-vbadditions-plugin.rb)
* [*workshop/multiple-slaves*](/rancher-os/workshop/multi-slave-rancheros/vagrant-rancheros-vbadditions-plugin.rb)


## Notes

* If you enable the *RSYNC* synced folders, you have to run `vagrant rsync-auto` as separate process with --no-polling (it makes it faster) option to enable the vagrant management of the synced folders watch.
* If you prefer to use *SSHFS* as the [*workshop*](/rancher-os/workshop), simply touch a file named : .ssfs_enable, to enable the vagrant ruby plugin to install the SSHFS guest protocol, on RancherOS (in this case be sure you have installed the SSHFS vagrant plugin behalf the script [init-vagrant-plugins.sh](/init-vagrant-plugins.sh))
* The NFS sync should have been provided by the NFS server, take in mind if you use this protocol you have to modify the Vagrantfile, and put the switch case, in order to provide the base protocol options


## Workshop

An example of RancherOS Server, a multiple RancherOS slave and 2 simple ubuntu dockerized vagrant init files are available in the provided :

[*Workshop*](/rancher-os/workshop)


## Contributors

You can access to the issue tracker at:

[Vagrant Development Issue Tracker](https://bitbucket.org/fabriziotorelli/vagrant-development/issues)


## License

[MIT License](/LICENSE.md)
