module VagrantPlugins
  module GuestLinux
    class Guest < Vagrant.plugin("2", :guest)
      # Name used for guest detection
      GUEST_DETECTION_NAME = "rancheros".freeze

      def detect?(machine)
        machine.communicate.test <<-EOH.gsub(/^ */, '')
          source /etc/os-release && test x#{self.class.const_get(:GUEST_DETECTION_NAME)} = x$ID && exit
          exit 1
        EOH
      end
    end
  end
end
