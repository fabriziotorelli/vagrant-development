# Rancher OS Workshop

## Synopsis

The goal of the workshop is to demonstrate the fast infrastructure development and the effectively flexibility of the Rancher Server and the Rancher-OS.

This workshop is part of the project [rancher-os](/rancher-os)

## Procedure

It is provided at the moment :

* Base Rancher OS on disk writable operating system vagrant folder at [rancheros-server](/rancher-os/workshop/rancheros-server)
* Sample single and multiple instance Vagrantfile folders available at [ubuntu-docker-machine-1](/rancher-os/workshop/ubuntu-docker-machine-1) and [ubuntu-docker-machine-2](/rancher-os/workshop/ubuntu-docker-machine-2)
* Following instructions
* Workshop including one RancherOS System and a couple of ubuntu docker machines based on the box created in the main project area [Ubuntu Docker Vagrant Machine](https://bitbucket.org/fabriziotorelli/vagrant-development.git)

## Prerequisites

The installation requires Virtual Box, Vagrant, the Virtual Box Guest Additions.

Installation or presence of sshfs in the infrastructure allows a development purpose, however it's not required.

For os-x :
```
brew cask install osxfuse
brew install homebrew/fuse/sshfs
```

For Linux: should be provided with OpenSSH

For Windows : Install cygwin -> openSSH


## Preparation

Ensure to have installed Virtual Box 5.1.XX, the Virtual Box Guest Additions 5.1.XX and Vagrant from the mac-sw folder. Ensure to have installed the base boxes for for ubuntu and the row box for Rancher OS in the vagrant framework (see [Rancher OS Installation Guide](/rancher-os)).



## Workshop

The steps:

* Power on the Rancher OS vagrant machine in [rancheros-server](/rancher-os/workshop/rancheros-server) with the command `vagrant up`;
* Reload the machine to apply in vagrant all the network changes with command `vagrant reload`;
* Login the Rancher OS server using the command `vagrant ssh`;
* create an [hosts](/rancher-os/workshop/rancheros-server/rancher-server/hosts) file in the */root* folder (*as root*), the same as the sample one and change the rancher name ip with the current server machine (the machine you are using to run vagrant) one [create manually].
* Install the Rancher Server with the following command : `sudo docker run -d --hostname=localhost --name rancher-server --privileged --restart=unless-stopped -v /root/hosts:/etc/hosts -p 8080:8080 rancher/server` (in order to verify the installation you can run the command `sudo docker logs -f rancher-server`), alternatively there is available the script [create-rancher-server.sh](/rancher-os/workshop/rancheros-server/rancher-server/create-rancher-server.sh) in the r-synced folder */opt/rancher/rancher-server*;
* Now you can open the browser and connect to the server (http://<localhost or your machine name/ip>:8080/)
* Start up the two ubuntu machines at the folders: [ubuntu-docker-machine-1](/rancher-os/workshop/ubuntu-docker-machine-1) and [ubuntu-docker-machine-2](/rancher-os/workshop/ubuntu-docker-machine-2) with the command `vagrant up`;
* Access with ssh to the machine [ubuntu-docker-machine-1](/rancher-os/workshop/ubuntu-docker-machine-1) running the command `vagrant ssh`;
* From the Rancher Server Web Console select Settings and change the server host as follow : *http://<localhost or your machine name/ip>:8080/*;
* Now from the Rancher Server Web Console select Infrastructure/Hosts and press the button Add Host and copy the agent console string and paste in the ubuntu-docker-machine-1 ssh console (*NOTE: Your have to run it as superuser and you should ensure the command in preceded by the command 'sudo'*);
* In a few minutes the host appears in the Hosts List Page (you can press close on the Add Server Page);
* You can change the Server meta data in order to name, description, tags, and so on ... or control the services;
* In the ssh console of the host ubuntu-docker-machine-1 you can access to the '/vagrant' folder and start creating the [NFS4 Server](/rancher-os/workshop/ubuntu-docker-machine-1/nfs4-server) docker image running the script [create-docker-image.sh](/rancher-os/workshop/ubuntu-docker-machine-1/nfs4-server/create-docker-image.sh) accessing to the folder [/vagrant/nfs4-server](/rancher-os/workshop/ubuntu-docker-machine-1/nfs4-server);
* Now you can run the compose to create the service with the command line `sudo docker-compose up` in the folder [/vagrant/nfs4-server/sample](/rancher-os/workshop/ubuntu-docker-machine-1/nfs4-server/sample) or on the rancher server console adding a Stack and using the [/vagrant/nfs4-server/sample/docker-compose.yml](/rancher-os/workshop/ubuntu-docker-machine-1/nfs4-server/sample/docker-compose.yml) from the same folder and using the [/vagrant/nfs4-server/sample/rancher-compose.yml](/rancher-os/workshop/ubuntu-docker-machine-1/nfs4-server/sample/rancher-compose.yml);
* Now you can test your NFS4 service from the ubuntu-docker-machine-1 using the test script [/vagrant/nfs4-server/sample/test_mount.sh](/rancher-os/workshop/ubuntu-docker-machine-1/nfs4-server/sample/test_mount.sh)
* Now you can go to the Rancher Server Web Console and from the repository install the 'Rancher NFS' Stack using as host the private network : `192.168.50.115`, mount point: `/`, options: `proto=tcp,port=2049` and version : `4` (It's suitable a transactional NFS Rancher Service named [Convoy NFS](https://github.com/rancher/convoy) not in scope in this workshop, very used in production environments, with an elastic management and powerful space re-allocation and sync);
* Now in the Infrastructure/Storage you can find the early configured NFS Server Storage Stack Informations;
* You can add a sample MySQL database whom writes volumes on the nfs using the configuration files in [/rancher-os/workshop/rancheros-server/sample](/rancher-os/workshop/rancheros-server/sample)
* Now on the environments (....) you can rename the current one and add another named : `Kubernetes` with the driver `Kubernetes`;
* Now you can switch to Kubernetes Environment from the Environment Drop-Down (in the left-top of the page);
* To complete the envoronment configuration you have to add an host (ubuntu-docker-machine-2) as explained for the host ubuntu-docker-machine-2;
* When the configuration is completed you can add a stack (eg. WordPress) from the catalog and you can access to the Kubernet UI and check/alter the Kubernetes service configuration;


## Available Configuration sample

For a visual procedure to how-to define a simple Rancher Server configuration and deploy a stack you can refer to the Visual Guide on [Drop Box](https://www.dropbox.com/work/Dublin/WorkShops/RancherOs), including:

* Generic Rancher Server Web Admin Configuration Guide;

* AWS Specific Rancher Server Web Admin and Platform Configuration Guide;



## Notes

* If you enable the *RSYNC* synced folders, you have to run `vagrant rsync-auto` as separate process with --no-polling (it makes it faster) option to enable the vagrant management of the synced folders watch.
* If you prefer to use *SSHFS* as the workshop, simply touch a file named : .ssfs_enable, to enable the vagrant ruby plugin to install the SSHFS guest protocol, on RancherOS (in this case be sure you have installed the SSHFS vagrant plugin behalf the script [init-vagrant-plugins.sh](/init-vagrant-plugins.sh))
* The NFS sync should have been provided by the NFS server, take in mind if you use this protocol you have to modify the Vagrantfile, and put the switch case, in order to provide the base protocol options


## Contributors

You can access to the issue tracker at:

[Vagrant Development Issue Tracker](https://bitbucket.org/fabriziotorelli/vagrant-development/issues)

## License

[MIT License](/LICENSE.md)
