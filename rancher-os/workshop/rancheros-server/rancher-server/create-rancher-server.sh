#!/bin/bash
sudo cp ./hosts /root/
if ! [[ -z "$(sudo docker ps -a | grep rancher-server)" ]]; then
  echo "Destroying existing  rancher-server container ..."
  sudo docker stop rancher-server
  sudo docker rm rancher-server
  echo "Now we proceed with the rancher server installation ..."
else
  echo "Ready to install racher-server..."
fi
sudo docker run -d --hostname=localhost --name rancher-server --privileged --restart=unless-stopped -v /root/hosts:/etc/hosts -p 8080:8080 rancher/server
sudo docker logs -f rancher-server
